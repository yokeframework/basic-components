<?php
// yokecore公共函数

use base\ServiceContainer;

/**
 * 返回正常结果
 * @param  array  $params  返回信息参数
 * @return [type]         [description]
 */
function return_success($params=[]){
	if(!is_array($params)){
        return ;
    }

    $result['data']=isset($params['data'])?$params['data']:[];
    $result['info']=isset($params['info'])?$params['info']:'ok';
    $result['status']=isset($params['status'])?$params['status']:1;


    return json($result);
}


/**
 * 异常返回
 * @param  array $param  异常信息
 * @param  string $action 异常类型
 * @return [type]         [description]
 */
function return_error($param,$action='Miss'){

	$name='\\base\\exception\\'.$action.'Exception';

    throw new $name($param);
    
}

/**
 * 计算时间
 *
 * @param datetime $the_time
 * @return void
 */
function time_tran($the_time){
    $now_time = time();
    $show_time = strtotime($the_time);
    $dur = $now_time - $show_time;
    if($dur < 0){
        return '刚刚'; 
    }else{
        if($dur < 60){
            return $dur.'秒前'; 
        }else{
            if($dur < 3600){
                return floor($dur/60).'分钟前'; 
            }else{
                if($dur < 86400){
                    return floor($dur/3600).'小时前'; 
                }else{
                    if($dur < 259200){//3天内
                        return floor($dur/86400).'天前';
                    }else{
                        return date('m-d',$show_time); 
                    }
                }
            }
        }
    }
}

/**
 * 实例化应用服务
 *
 * @param string $name
 * @return object
 */
function service($name,$caller='',$namespace='')
{
    if(empty($caller)){
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS,2);
        $caller = str_replace(["app\\","controller\\"],['',''],$trace[1]['class']).'.'.$trace[1]['function'];
    }

    return ServiceContainer::initInstance($name,$caller,$namespace);
}

/**
 * 实例化应用逻辑
 *
 * @param string $name
 * @return object
 */
function yk_logic($name)
{
    $arr = explode('\\',$name);

    if(empty($arr)){
        return_error(['info'=>'实例化逻辑名称不能为空']);
    }

    $len = count($arr);

    if($len>1){
        $arr[$len-1]=ucfirst($arr[$len-1])."Logic";
        $class = implode("\\",$arr);
    }else{
        $class = ucfirst($arr[0])."Logic";
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS,2);
        $namespace = explode('\\',$trace[1]['class']);

        $pos = array_search('controller',$namespace);

        if(!$pos){
            return_error(['info'=>'调用层级异常']);
        }
        $namespace = array_slice($namespace,0,$pos);
        array_push($namespace,'logic',$class);
        $class = implode('\\',$namespace);

    }

    if(class_exists($class)){
        return new $class();
    }

    return_error(['info'=>$class.'不存在']);

}

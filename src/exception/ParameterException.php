<?php
namespace base\exception;

/**
 * 通用参数类异常错误
 * zq 2018年4月2日 
 */
class ParameterException extends BaseException
{
    public $code = 200;
    public $errorCode = 0;
    public $msg = "invalid parameters";
}
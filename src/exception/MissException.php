<?php
namespace base\exception;

/**
 * 404时抛出此异常
 * zq 2018年4月2日
 */
class MissException extends BaseException
{
    public $code = 200;
    public $msg = 'global:your required resource are not found';
    public $errorCode = 0;
}
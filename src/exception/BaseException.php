<?php
namespace base\exception;
use think\Exception;

Class BaseException extends Exception{
    public $code=200;
    public $msg='invalid parameters';
    public $errorCode=0;
    
    
    /**
     * 自定义异常类的基类
     * zq 2018年4月2日
     */
    public function __construct($params=[]){
        if(!is_array($params)){
            return ;
        }

        if(array_key_exists('code', $params)){
            $this->code=$params['code'];
        }
        if(array_key_exists('info', $params)){
            $this->msg=$params['info'];
            $this->message=$params['info'];
        }
        if(array_key_exists('status', $params)){
            $this->errorCode=$params['status'];
        }
    }
}
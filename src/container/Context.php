<?php
namespace base\container;

use base\ServiceContainer;

/**
 * 上下文信息传递
 */
class Context
{
    /**
     * 逻辑服务容器
     * @var ServiceContainer
     */
    private $container;

    /**
     * 上下文信息
     * @var array
     */
    private $context = [];

    /**
     * 构造函数初始化
     * @param ServiceContainer $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }
    
    /**
     * 获取当前容器
     *
     * @return void
     */
    public function container()
    {
        return $this->container;
    }

    /**
     * 获取调用者
     *
     * @return void
     */
    public function caller()
    {
        return $this->container->caller;
    }

    /**
     * 设置上下文信息
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set(string $name,$value)
    {
        $this->context[$name]=$value;
    }

    /**
     * 获取上下文信息
     *
     * @param string $name
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->context[$name]??null;
    }

    /**
     * 获取所有上下文信息
     * @return void
     */
    public function getContext()
    {
        return $this->context;
    }

}